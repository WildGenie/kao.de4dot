Description
===========

de4dot is an open source (GPLv3) .NET deobfuscator and unpacker written in C#. It will try its best to restore a packed and obfuscated assembly to almost the original assembly. Most of the obfuscation can be completely restored (eg. string encryption), but symbol renaming is impossible to restore since the original names aren't (usually) part of the obfuscated assembly.


Modifications
=============

This is modified version of de4dot. If you're curious what was changed, look at the commit logs.
There will be no compiled version available. If you can't compile it, you don't need it.


Thanks
======

Thank you all for helping me by sending bugreports, patches and useful comments.
In no particular order: li0nsar3c00l, NOP, Apocalypse, Erjey.